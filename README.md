##What does this project stand for?
It is an simple application to publish news, view them in various formats and to provide basic CRUD features.

##Tech Stack
1. Spring boot in back end 
2. MongoDB as repository
3. AngularJS in front end
4. Designed as single page application
5. Maven for back end dependency management and bower for front end dependency management
6. Bootstrap for UI design and layout